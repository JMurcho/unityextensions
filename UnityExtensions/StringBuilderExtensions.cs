﻿using System.Text;

namespace UnityExtensions
{
    public static class StringBuilderExtensions
    {
        /// <summary>
        /// Clears the currently stored string from the StringBuilder.  This is implemented in
        /// .NET 4.0, so remove once mono is updated accordingly.
        /// </summary>
        /// <param name="sb">StringBuilder object to clear</param>
        public static void Clear(this StringBuilder sb)
        {
            sb.Length = 0;
        }
    }
}
