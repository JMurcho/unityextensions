﻿using UnityEngine;

namespace UnityExtensions
{
    public static class GameObjectExtensions
    {
        /// <summary>
        /// Sets the GameObject's layer, and all child GameObjects too 
        /// </summary>
        /// <param name="go">The GameObject to set</param>
        /// <param name="layer">The new layer to set</param>
        public static void SetLayerRecursively(this GameObject go, int layer)
        {
            go.layer = layer;
            go.transform.SetLayerRecursively(layer);
        }

        /// <summary>
        /// Sets the GameObject's layer and all child GameObjects too
        /// </summary>
        /// <param name="go">The GameObject to set</param>
        /// <param name="layer">The new layer to set</param>
        public static void SetLayerRecursively(this GameObject go, string layer)
        {
            int layerInt = LayerMask.NameToLayer(layer);
            go.SetLayerRecursively(layerInt);
        }
    }
}
