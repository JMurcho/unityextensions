﻿using UnityEngine;

namespace UnityExtensions
{
    public static class MathfExtensions
    {
        /// <summary>
        /// Converts a measurement in Centimeters to Inches
        /// </summary>
        /// <param name="cm">The measurement in Centimeters</param>
        /// <returns>The measurement in Inches</returns>
        public static float CMToInches(float cm)
        {
            return cm / 0.393700787f;
        }

        /// <summary>
        /// Converts a measurement in Inches to Centimeters
        /// </summary>
        /// <param name="inches">The measurement in Inches</param>
        /// <returns>The measurement in Centimeters</returns>
        public static float InchesToCM(float inches)
        {
            return inches * 0.393700787f;
        }
    }
}
