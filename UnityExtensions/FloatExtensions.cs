﻿namespace UnityExtensions
{
    public static class FloatExtensions
    {
        /// <summary>
        /// Performs a range check on the float.
        /// </summary>
        /// <param name="a">Current float being tested</param>
        /// <param name="b">Other float to compare to</param>
        /// <param name="threshold">Range the value can be within above or below</param>
        /// <returns>Is this float approximately the same os the other given float</returns>
        public static bool IsApproximately(this float a, float b, float threshold)
        {
            return (a >= b - threshold && a <= b + threshold);
        }
    }
}
