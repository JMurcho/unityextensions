﻿using System.Collections.Generic;

namespace UnityExtensions
{
    public static class ListExtensions
    {
        /// <summary>
        /// Gets the last item in the list, or returns the default value if the list is empty
        /// </summary>
        /// <typeparam name="T">Type of object in the list</typeparam>
        /// <param name="list">The list to fetch from</param>
        /// <returns>The last item in the list, or default(T) if the list is empty</returns>
        public static T GetLast<T>(this List<T> list)
        {
            return list.Count > 0 ? list[list.Count - 1] : default(T);
        }

        /// <summary>
        /// Removes the last item in the list, if there are any items in the list
        /// </summary>
        /// <typeparam name="T">Type of object in the list</typeparam>
        /// <param name="list">The list to remove from</param>
        public static void RemoveLast<T>(this List<T> list)
        {
            if(list.Count > 0)
                list.RemoveAt(list.Count - 1);
        }

        /// <summary>
        /// Gets the first item in the list, or returns the default value if the list is empty
        /// </summary>
        /// <typeparam name="T">The type of object in the list</typeparam>
        /// <param name="list">The list to fetch from</param>
        /// <returns>The first item in the list, or default(T) if the list is empty</returns>
        public static T GetFirst<T>(this List<T> list)
        {
            return list.Count > 0 ? list[0] : default(T);
        }

        /// <summary>
        /// Removes the first item in the list, if there are any itmes in the list
        /// </summary>
        /// <typeparam name="T">Type of object in the list</typeparam>
        /// <param name="list">The list to remove from</param>
        public static void RemoveFirst<T>(this List<T> list)
        {
            if (list.Count > 0)
                list.RemoveAt(0);
        }
    }
}
