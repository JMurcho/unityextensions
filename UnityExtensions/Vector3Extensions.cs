using UnityEngine;

namespace UnityExtensions
{
    public static class Vector3Extensions
    {
        /// <summary>
        /// Returns the X and Y components of the Vector3 as a Vector2
        /// </summary>
        /// <param name="vec">The current Vector3</param>
        /// <returns>Packed Vector2</returns>
        public static Vector2 GetXY(this Vector3 vec)
        {
            return new Vector2(vec.x, vec.y);
        }

        /// <summary>
        /// Returns the Y and X components of the Vector3 as a Vector2
        /// </summary>
        /// <param name="vec">The current Vector3</param>
        /// <returns>Packed Vector2</returns>
        public static Vector2 GetYX(this Vector3 vec)
        {
            return new Vector2(vec.y, vec.x);
        }

        /// <summary>
        /// Returns the X and Z components of the Vector3 as a Vector2
        /// </summary>
        /// <param name="vec">The current Vector3</param>
        /// <returns>Packed Vector2</returns>
        public static Vector2 GetXZ(this Vector3 vec)
        {
            return new Vector2(vec.x, vec.z);
        }

        /// <summary>
        /// Returns the Z and X components of the Vector3 as a Vector2
        /// </summary>
        /// <param name="vec">The current Vector3</param>
        /// <returns>Packed Vector2</returns>
        public static Vector2 GetZX(this Vector3 vec)
        {
            return new Vector2(vec.z, vec.x);
        }

        /// <summary>
        /// Returns the Y and Z components of the Vector3 as a Vector2
        /// </summary>
        /// <param name="vec">The current Vector3</param>
        /// <returns>Packed Vector2</returns>
        public static Vector2 GetYZ(this Vector3 vec)
        {
            return new Vector2(vec.y, vec.z);
        }

        /// <summary>
        /// Returns the Y and Z components of the Vector3 as a Vector2
        /// </summary>
        /// <param name="vec">The current Vector3</param>
        /// <returns>Packed Vector2</returns>
        public static Vector2 GetZY(this Vector3 vec)
        {
            return new Vector2(vec.z, vec.y);
        }
    }
}
