﻿using UnityEngine;

namespace UnityExtensions
{
    public static class TransformExtensions
    {
        #region Positions
        /// <summary>
        /// Sets the X position of the transform
        /// </summary>
        /// <param name="t">The transform to set</param>
        /// <param name="xPos">New x position</param>
        public static void SetX(this Transform t, float xPos)
        {
            var pos = t.position;
            pos.x = xPos;
            t.position = pos;
        }

        /// <summary>
        /// Sets the Y position of the transform
        /// </summary>
        /// <param name="t">The transform to set</param>
        /// <param name="yPos">New y position</param>
        public static void SetY(this Transform t, float yPos)
        {
            var pos = t.position;
            pos.y = yPos;
            t.position = pos;
        }

        /// <summary>
        /// Sets the Z position of the transform
        /// </summary>
        /// <param name="t">The transform to set</param>
        /// <param name="zPos">New z position</param>
        public static void SetZ(this Transform t, float zPos)
        {
            var pos = t.position;
            pos.z = zPos;
            t.position = pos;
        }

        /// <summary>
        /// Sets the local X position of the transform
        /// </summary>
        /// <param name="t">The transform to set</param>
        /// <param name="xPos">New local x position</param>
        public static void SetLocalX(this Transform t, float xPos)
        {
            var pos = t.localPosition;
            pos.x = xPos;
            t.position = pos;
        }

        /// <summary>
        /// Sets the local Y position of the transform
        /// </summary>
        /// <param name="t">The transform to set</param>
        /// <param name="yPos">New local y position</param>
        public static void SetLocalY(this Transform t, float yPos)
        {
            var pos = t.localPosition;
            pos.y = yPos;
            t.position = pos;
        }

        /// <summary>
        /// Sets the local Z position of the transform
        /// </summary>
        /// <param name="t">The transform to set</param>
        /// <param name="zPos">New local z position</param>
        public static void SetLocalZ(this Transform t, float zPos)
        {
            var pos = t.localPosition;
            pos.z = zPos;
            t.position = pos;
        }

        /// <summary>
        /// Moves the X Position of the transform
        /// </summary>
        /// <param name="t">The transform to move</param>
        /// <param name="xMove">Value to move by in the X Axis</param>
        public static void MoveX(this Transform t, float xMove)
        {
            var pos = t.position;
            pos.x += xMove;
            t.position = pos;
        }

        /// <summary>
        /// Moves the Y Position of the transform
        /// </summary>
        /// <param name="t">The transform to move</param>
        /// <param name="yMove">Value to move by in the Y Axis</param>
        public static void MoveY(this Transform t, float yMove)
        {
            var pos = t.position;
            pos.y += yMove;
            t.position = pos;
        }

        /// <summary>
        /// Moves the Z Position of the transform
        /// </summary>
        /// <param name="t">The transform to move</param>
        /// <param name="zMove">Value to move by in the Z Axis</param>
        public static void MoveZ(this Transform t, float zMove)
        {
            var pos = t.position;
            pos.z += zMove;
            t.position = pos;
        }
		
		/// <summary>
		/// Moves the X and Y positions of the transform by the X and Y components of move.
		/// </summary>
		/// <param name='t'>
		/// The tranform to move
		/// </param>
		/// <param name='move'>
		/// Value to move X and Y by
		/// </param>
		public static void MoveXY(this Transform t, Vector2 move)
		{
			var pos = t.position;
			pos.x += move.x;
			pos.y += move.y;
			t.position = pos;
		}
		
		/// <summary>
		/// Moves the Y and X positions of the transform by the X and Y components of move.
		/// </summary>
		/// <param name='t'>
		/// The tranform to move
		/// </param>
		/// <param name='move'>
		/// Value to move Y and X by
		/// </param>
		public static void MoveYX(this Transform t, Vector2 move)
		{
			var pos = t.position;
			pos.y += move.x;
			pos.x += move.y;
			t.position = pos;
		}
		
		/// <summary>
		/// Moves the X and Z positions of the transform by the X and Y components of move.
		/// </summary>
		/// <param name='t'>
		/// The tranform to move
		/// </param>
		/// <param name='move'>
		/// Value to move X and Z by
		/// </param>
		public static void MoveXZ(this Transform t, Vector2 move)
		{
			var pos = t.position;
			pos.x += move.x;
			pos.z += move.y;
			t.position = pos;
		}
		
		/// <summary>
		/// Moves the Z and X positions of the transform by the X and Y components of move.
		/// </summary>
		/// <param name='t'>
		/// The tranform to move
		/// </param>
		/// <param name='move'>
		/// Value to move Z and X by
		/// </param>
		public static void MoveZX(this Transform t, Vector2 move)
		{
			var pos = t.position;
			pos.z += move.x;
			pos.x += move.y;
			t.position = pos;
		}
		
		/// <summary>
		/// Moves the Y and Z positions of the transform by the X and Y components of move.
		/// </summary>
		/// <param name='t'>
		/// The tranform to move
		/// </param>
		/// <param name='move'>
		/// Value to move Y and Z by
		/// </param>
		public static void MoveYZ(this Transform t, Vector2 move)
		{
			var pos = t.position;
			pos.y += move.x;
			pos.z += move.y;
			t.position = pos;
		}
		
		/// <summary>
		/// Moves the Z and Y positions of the transform by the X and Y components of move.
		/// </summary>
		/// <param name='t'>
		/// The tranform to move
		/// </param>
		/// <param name='move'>
		/// Value to move Z and Y by
		/// </param>
		public static void MoveZY(this Transform t, Vector2 move)
		{
			var pos = t.position;
			pos.z += move.x;
			pos.y += move.y;
			t.position = pos;
		}
		
        #endregion

        #region Rotations
        /// <summary>
        /// Sets the X Rotation (euler) for the transform
        /// </summary>
        /// <param name="t">The transform to set</param>
        /// <param name="xRot">New X Rotation</param>
        public static void SetXRot(this Transform t, float xRot)
        {
            var rot = t.rotation;
            rot.x = xRot;
            t.rotation = rot;
        }

        /// <summary>
        /// Sets the Y Rotation (euler) for the transform
        /// </summary>
        /// <param name="t">The transform to set</param>
        /// <param name="yRot">New Y Rotation</param>
        public static void SetYRot(this Transform t, float yRot)
        {
            var rot = t.rotation;
            rot.y = yRot;
            t.rotation = rot;
        }

        /// <summary>
        /// Sets the Z Rotation (euler) for the transform
        /// </summary>
        /// <param name="t">The transform to set</param>
        /// <param name="zRot">New Z Rotation</param>
        public static void SetZRot(this Transform t, float zRot)
        {
            var rot = t.rotation;
            rot.z = zRot;
            t.rotation = rot;
        }

        /// <summary>
        /// Sets the Local X Rotation (euler) for the transform
        /// </summary>
        /// <param name="t">The transform to set</param>
        /// <param name="xRot">New X Rotation</param>
        public static void SetLocalXRot(this Transform t, float xRot)
        {
            var rot = t.localRotation;
            rot.x = xRot;
            t.localRotation = rot;
        }

        /// <summary>
        /// Sets the Local Y Rotation (euler) for the transform
        /// </summary>
        /// <param name="t">The transform to set</param>
        /// <param name="yRot">New Y Rotation</param>
        public static void SetLocalYRot(this Transform t, float yRot)
        {
            var rot = t.localRotation;
            rot.y = yRot;
            t.localRotation = rot;
        }

        /// <summary>
        /// Sets the Local Z Rotation (euler) for the transform
        /// </summary>
        /// <param name="t">The transform to set</param>
        /// <param name="zRot">New Z Rotation</param>
        public static void SetLocalZRot(this Transform t, float zRot)
        {
            var rot = t.localRotation;
            rot.z = zRot;
            t.localRotation = rot;
        }

        #endregion

        #region Scale

        /// <summary>
        /// Sets the X scale for the transform
        /// </summary>
        /// <param name="t">The transform to set</param>
        /// <param name="xScale">New X Scale</param>
        public static void SetXScale(this Transform t, float xScale)
        {
            var scale = t.localScale;
            scale.x = xScale;
            t.localScale = scale;
        }

        /// <summary>
        /// Sets the Y scale for the transform
        /// </summary>
        /// <param name="t">The transform to set</param>
        /// <param name="yScale">New Y Scale</param>
        public static void SetYScale(this Transform t, float yScale)
        {
            var scale = t.localScale;
            scale.y = yScale;
            t.localScale = scale;
        }

        /// <summary>
        /// Sets the Z scale for the transform
        /// </summary>
        /// <param name="t">The transform to set</param>
        /// <param name="zScale">New Z Scale</param>
        public static void SetZScale(this Transform t, float zScale)
        {
            var scale = t.localScale;
            scale.z = zScale;
            t.localScale = scale;
        }

        #endregion

        /// <summary>
        /// Sets the Transform's layer, and all child transforms as well.
        /// </summary>
        /// <param name="t">The transfrom to set</param>
        /// <param name="layer">The new layer to set</param>
        public static void SetLayerRecursively(this Transform t, int layer)
        {
            t.gameObject.layer = layer;
            foreach (Transform child in t)
            {
                child.SetLayerRecursively(layer);
            }
        }

        /// <summary>
        /// Sets the Transform's layer, and all child transforms as well
        /// </summary>
        /// <param name="t">The transform to set</param>
        /// <param name="layer">The new layer to set</param>
        public static void SetLayerRecursively(this Transform t, string layer)
        {
            var layerInt = LayerMask.NameToLayer(layer);
            t.SetLayerRecursively(layerInt);
        }

        /// <summary>
        /// Gets all sibling transforms linked to this transform's parent
        /// </summary>
        /// <param name="t">The Transform to get siblings for</param>
        /// <returns>Transform array of all sibling objects</returns>
        public static Transform[] GetSiblings(this Transform t)
        {
            if (t.parent == null)
                return null;

            var siblings = new Transform[t.parent.childCount - 1];
            bool foundSelf = false;
            for(int i = 0; i < t.parent.childCount; i++)
            {
                if(t.parent.GetChild(i) == t)
                {
                    foundSelf = true;
                    continue;
                }
                siblings[foundSelf ? i - 1 : i] = t.parent.GetChild(i);
            }
            return siblings;
        }

        /// <summary>
        /// Is this transform near another transform?
        /// </summary>
        /// <param name="t">The current Transform to check</param>
        /// <param name="other">The other Transform to test against</param>
        /// <param name="maxDistance">Maximum distance considered near</param>
        /// <returns>True if inside maxDistance</returns>
        public static bool IsNear(this Transform t, Transform other, float maxDistance)
        {
            return IsNear(t, other.position, maxDistance);
        }

        /// <summary>
        /// Is this transform near a point?
        /// </summary>
        /// <param name="t">The current Transform to check</param>
        /// <param name="point">The point to test against</param>
        /// <param name="maxDistance">Maximum distance considered near</param>
        /// <returns>True if inside maxDistance</returns>
        public static bool IsNear(this Transform t, Vector3 point, float maxDistance)
        {
            return (point - t.position).sqrMagnitude < (maxDistance*maxDistance);
        }

        /// <summary>
        /// Is this transform far from another transform
        /// </summary>
        /// <param name="t">The current Transform to check</param>
        /// <param name="other">The other Transform to test against</param>
        /// <param name="minDistance">The minimum distance considered far</param>
        /// <returns>True if outside minDistance</returns>
        public static bool IsFar(this Transform t, Transform other, float minDistance)
        {
            return !IsNear(t, other.position, minDistance);
        }

        /// <summary>
        /// Is this transform far from a point
        /// </summary>
        /// <param name="t">The current Transform to check</param>
        /// <param name="point">The other point to test against</param>
        /// <param name="minDistance">The minimum distance considered far</param>
        /// <returns>True if outside minDistance</returns>
        public static bool IsFar(this Transform t, Vector3 point, float minDistance)
        {
            return !IsNear(t, point, minDistance);
        }
    }
}
