﻿using UnityEngine;

namespace UnityExtensions
{
    public static class Vector2Extensions
    {
        /// <summary>
        /// Rotates a vector by a given amount
        /// </summary>
        /// <param name="original">The current Vector2 to rotate</param>
        /// <param name="rotAmount">The rotation angle in Radians</param>
        /// <returns>The rotated Vector2</returns>
        public static Vector2 RotateVector(this Vector2 original, float rotAmount)
        {
            var retVec = Vector2.zero;
            retVec.x = original.x * Mathf.Cos(rotAmount) - original.y * Mathf.Sin(rotAmount);
            retVec.y = original.x * Mathf.Sin(rotAmount) + original.y * Mathf.Cos(rotAmount);
            return retVec;
        }
    }
}
